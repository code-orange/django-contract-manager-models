from django.core.management.base import BaseCommand

from django.conf import settings

from django_contract_manager_models.django_contract_manager_models.models import *
from django_sap_business_one_models.django_sap_business_one_models.models import (
    DolCtrContracts,
)


class Command(BaseCommand):
    help = "Migrate TOP level contracts"

    def handle(self, *args, **options):
        all_top_contracts = DolCtrContracts.objects.filter(
            u_subcontractofid__isnull=True,
            # u_dateend__isnull=True,
        )

        for top_contract in all_top_contracts:
            if top_contract.u_artnr.itemcode.startswith("DTELPLAN"):
                try:
                    contract_customer = ContractCustTypes(
                        customer=MdatCustomers.objects.get(
                            created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                            external_id=str(top_contract.u_custnr.cardcode),
                        ),
                        contract_type_id=1,
                        contract_int_id=int(top_contract.code),
                    )
                    contract_customer.save()
                except:
                    pass

            if top_contract.u_artnr.itemcode.startswith("12DSL"):
                try:
                    contract_customer = ContractCustTypes(
                        customer=MdatCustomers.objects.get(
                            created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                            external_id=str(top_contract.u_custnr.cardcode),
                        ),
                        contract_type_id=2,
                        contract_int_id=int(top_contract.code),
                    )
                    contract_customer.save()
                except:
                    pass

            if top_contract.u_artnr.itemcode.startswith("HSWEB"):
                try:
                    contract_customer = ContractCustTypes(
                        customer=MdatCustomers.objects.get(
                            created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                            external_id=str(top_contract.u_custnr.cardcode),
                        ),
                        contract_type_id=3,
                        contract_int_id=int(top_contract.code),
                    )
                    contract_customer.save()
                except:
                    pass

            if top_contract.u_artnr.itemcode == "DOLC001":
                try:
                    contract_customer = ContractCustTypes(
                        customer=MdatCustomers.objects.get(
                            created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                            external_id=str(top_contract.u_custnr.cardcode),
                        ),
                        contract_type_id=4,
                        contract_int_id=int(top_contract.code),
                    )
                    contract_customer.save()
                except:
                    pass

            if top_contract.u_artnr.itemcode == "DOLSEC001":
                try:
                    contract_customer = ContractCustTypes(
                        customer=MdatCustomers.objects.get(
                            created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                            external_id=str(top_contract.u_custnr.cardcode),
                        ),
                        contract_type_id=5,
                        contract_int_id=int(top_contract.code),
                    )
                    contract_customer.save()
                except:
                    pass

            # if top_contract.u_artnr.itemcode == 'HSVSBTO':
            #     try:
            #         contract_customer = ContractCustTypes(
            #             customer=MdatCustomers.objects.get(
            #                 created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
            #                 external_id=str(top_contract.u_custnr.cardcode)
            #             ),
            #             contract_type_id=6,
            #             contract_int_id=int(top_contract.code)
            #         )
            #         contract_customer.save()
            #     except:
            #         pass

            if top_contract.u_artnr.itemcode == "DOLC002":
                try:
                    contract_customer = ContractCustTypes(
                        customer=MdatCustomers.objects.get(
                            created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                            external_id=str(top_contract.u_custnr.cardcode),
                        ),
                        contract_type_id=7,
                        contract_int_id=int(top_contract.code),
                    )
                    contract_customer.save()
                except:
                    pass

            if top_contract.u_artnr.itemcode.startswith("DOLSVC"):
                try:
                    contract_customer = ContractCustTypes(
                        customer=MdatCustomers.objects.get(
                            created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                            external_id=str(top_contract.u_custnr.cardcode),
                        ),
                        contract_type_id=8,
                        contract_int_id=int(top_contract.code),
                    )
                    contract_customer.save()
                except:
                    pass

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
