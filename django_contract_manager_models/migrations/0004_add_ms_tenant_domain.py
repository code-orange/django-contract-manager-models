# Generated by Django 3.1.13 on 2021-08-27 23:57

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_contract_manager_models", "0003_add_contract_to_customer_link"),
    ]

    operations = [
        migrations.AddField(
            model_name="contractcustsettings",
            name="ms_tenant_domain",
            field=models.CharField(max_length=200, null=True, unique=True),
        ),
    ]
