# Generated by Django 3.2.13 on 2022-05-06 11:33

import datetime
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("django_mdat_customer", "0042_add_mdat_item_customer_special_prices"),
        ("django_contract_manager_models", "0019_add_contract_terms_to_contracts"),
    ]

    operations = [
        migrations.CreateModel(
            name="ContractPartnerCommissions",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "commission_percent",
                    models.DecimalField(
                        decimal_places=10,
                        max_digits=13,
                        validators=[
                            django.core.validators.MinValueValidator(0),
                            django.core.validators.MaxValueValidator(100),
                        ],
                    ),
                ),
                ("date_added", models.DateTimeField(default=datetime.datetime.now)),
                ("start", models.DateField(default=datetime.date.today)),
                ("end_raw", models.DateField(default=None, null=True)),
                ("billed_until", models.DateField(default=datetime.date.today)),
                (
                    "contract",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="django_contract_manager_models.contracts",
                    ),
                ),
                (
                    "partner",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="django_mdat_customer.mdatcustomers",
                    ),
                ),
            ],
            options={
                "db_table": "contract_partner_commissions",
                "unique_together": {("contract", "partner")},
            },
        ),
    ]
