# Generated by Django 4.2.9 on 2024-04-26 10:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        (
            "django_contract_manager_models",
            "0026_translate_contract_types_and_billing_cycles",
        ),
    ]

    operations = [
        migrations.AlterField(
            model_name="contractbillingcyclestranslation",
            name="description",
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name="contracttypestranslation",
            name="description",
            field=models.CharField(max_length=50),
        ),
    ]
