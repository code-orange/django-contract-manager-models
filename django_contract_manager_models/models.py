from datetime import timedelta

from dateutil.relativedelta import relativedelta
from datetime import datetime
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.translation import gettext_lazy as _, ngettext_lazy
from parler.models import TranslatableModel, TranslatedFields

from django_mdat_customer.django_mdat_customer.models import *


class ContractCustSettings(models.Model):
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING, unique=True)
    ms_tenant_domain = models.CharField(max_length=200, null=True, unique=True)

    class Meta:
        db_table = "contract_cust_settings"


class ContractTypes(TranslatableModel):
    name = models.CharField(max_length=50, null=False, blank=False, unique=True)
    billing_cycle = models.ForeignKey("ContractBillingCycles", models.DO_NOTHING)

    translations = TranslatedFields(
        description=models.CharField(max_length=50, null=False, blank=False)
    )

    class Meta:
        db_table = "contract_types"


class ContractBillingCycles(TranslatableModel):
    name = models.CharField(max_length=100, unique=True)

    translations = TranslatedFields(
        description=models.CharField(max_length=50, null=False, blank=False)
    )

    class Meta:
        db_table = "contract_billing_cycles"


class ContractBundles(models.Model):
    item_src = models.ForeignKey(
        MdatItems, models.DO_NOTHING, related_name="bundle_item_src"
    )
    item_dst = models.ForeignKey(
        MdatItems, models.DO_NOTHING, related_name="bundle_item_dst"
    )
    quantity = models.IntegerField(default=1)

    class Meta:
        db_table = "contract_bundles"


class ContractCustTypes(models.Model):
    master_contract = models.OneToOneField(
        "Contracts", models.DO_NOTHING, primary_key=True
    )
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING)
    contract_type = models.ForeignKey(ContractTypes, models.DO_NOTHING)

    class Meta:
        db_table = "contract_cust_types"
        unique_together = (("customer", "contract_type"),)


class ContractTerms(models.Model):
    YEARS = "years"
    MONTHS = "months"
    DAYS = "days"
    HOURS = "hours"
    MINUTES = "minutes"
    SECONDS = "seconds"
    MICROSECONDS = "microseconds"

    PERIOD_CHOICES = (
        (YEARS, ngettext_lazy("Year", "Years", 2)),
        (MONTHS, ngettext_lazy("Month", "Months", 2)),
        (DAYS, ngettext_lazy("Day", "Days", 2)),
        (HOURS, ngettext_lazy("Hour", "Hours", 2)),
        (MINUTES, ngettext_lazy("Minute", "Minutes", 2)),
        (SECONDS, ngettext_lazy("Second", "Seconds", 2)),
        (MICROSECONDS, ngettext_lazy("Microsecond", "Microseconds", 2)),
    )

    minimum_contract_period_count = models.IntegerField()
    minimum_contract_period_unit = models.CharField(
        max_length=24, choices=PERIOD_CHOICES
    )

    termination_notice_contract_period_count = models.IntegerField()
    termination_notice_contract_period_unit = models.CharField(
        max_length=24, choices=PERIOD_CHOICES
    )

    auto_extend_contract_period_count = models.IntegerField()
    auto_extend_contract_period_unit = models.CharField(
        max_length=24, choices=PERIOD_CHOICES
    )

    @property
    def minimum_contract_period_delta(self):
        delta = self.calc_delta(
            self.minimum_contract_period_count, self.minimum_contract_period_unit
        )

        return delta

    @property
    def termination_notice_contract_period_delta(self):
        delta = self.calc_delta(
            self.termination_notice_contract_period_count,
            self.termination_notice_contract_period_unit,
        )

        return delta

    @property
    def auto_extend_contract_period_delta(self):
        return self.calc_delta(
            self.auto_extend_contract_period_count,
            self.auto_extend_contract_period_unit,
        )

    def calc_delta(self, count: int, unit: str):
        return relativedelta(**{unit: count})

    @property
    def minimum_contract_period(self):
        translation = self.get_contract_period_translation(
            self.minimum_contract_period_count,
            self.minimum_contract_period_unit,
        )

        return translation

    @property
    def auto_extend_contract_period(self):
        translation = self.get_contract_period_translation(
            self.auto_extend_contract_period_count,
            self.auto_extend_contract_period_unit,
        )

        return translation

    @property
    def termination_notice_contract_period(self):
        translation = self.get_contract_period_translation(
            self.termination_notice_contract_period_count,
            self.termination_notice_contract_period_unit,
        )

        return translation

    def get_contract_period_translation(self, count: int, unit: str):
        plural = unit.capitalize()
        singular = plural.removesuffix("s")

        unit_translation = ngettext_lazy(singular, plural, count)

        return f"{count} {unit_translation}"

    def __str__(self, short=False):
        text_long = _(
            "The minimum contract period is %s. If the contract is not terminated %s before the end of the current contract period, the contract is extended by %s."
        ) % (
            self.minimum_contract_period,
            self.termination_notice_contract_period,
            self.auto_extend_contract_period,
        )

        text_short = _(
            "Minimum term of %s, extended by %s, if not terminated %s prior to contract end."
        ) % (
            self.minimum_contract_period,
            self.auto_extend_contract_period,
            self.termination_notice_contract_period,
        )

        if short:
            return text_short

        return text_long

    class Meta:
        db_table = "contract_terms"
        unique_together = (
            (
                "minimum_contract_period_count",
                "minimum_contract_period_unit",
                "termination_notice_contract_period_count",
                "termination_notice_contract_period_unit",
                "auto_extend_contract_period_count",
                "auto_extend_contract_period_unit",
            ),
        )


class Contracts(DirtyFieldsMixin, models.Model):
    id = models.BigAutoField(primary_key=True)

    date_added = models.DateTimeField(default=datetime.now)

    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING)

    master_contract = models.ForeignKey(
        "self", models.DO_NOTHING, related_name="subcontracts", null=True, default=None
    )

    item = models.ForeignKey(MdatItems, models.DO_NOTHING)
    reference = models.CharField(max_length=100, blank=True, null=True, default=None)

    price = models.DecimalField(max_digits=19, decimal_places=6)

    start = models.DateField(default=date.today)
    last_renewal = models.DateField(default=date.today)
    end = models.DateField(null=True, default=None)
    billed_until = models.DateField(default=date.today)

    billing_cycle = models.ForeignKey(ContractBillingCycles, models.DO_NOTHING)
    contract_terms = models.ForeignKey(ContractTerms, models.DO_NOTHING)

    @property
    def discount_percent(self):
        # If item is free, always show 100% discount
        if self.price <= 0:
            return 100

        # If base item is free but not in contract, show no discount
        if self.item.price <= 0:
            return 0

        # If own price is higher than base price, show no discount
        if self.item.price < self.price:
            return 0

        # For all other cases: calculate discount
        result = 100 - (self.price / self.item.price * 100)

        return result

    @property
    def next_possible_end_of_the_contract_term(self):
        return_date = None
        minimum_contract_period = (
            self.start + self.contract_terms.minimum_contract_period_delta
        )
        renewal_contract_period = (
            self.last_renewal + self.contract_terms.auto_extend_contract_period_delta
        )

        if minimum_contract_period > renewal_contract_period:
            return_date = minimum_contract_period
        else:
            return_date = renewal_contract_period

        # End of contract term is last day of service, NOT the first day after
        return_date -= timedelta(days=1)

        for contract in Contracts.objects.filter(master_contract_id=self.id):
            sub_date = contract.next_possible_end_of_the_contract_term

            if sub_date > return_date:
                return_date = sub_date

        return return_date

    @property
    def receipt_of_the_notice_of_termination_no_later_than(self):
        return (
            self.next_possible_end_of_the_contract_term
            - self.contract_terms.termination_notice_contract_period_delta
        )

    def save(self, *args, **kwargs):
        if self.contract_terms is None and self.item is not None:
            self.contract_terms = self.item.contractitemterms.contract_terms

        super(Contracts, self).save(*args, **kwargs)

    class Meta:
        db_table = "contracts"


class ContractOneTimeBilling(models.Model):
    id = models.BigAutoField(primary_key=True)
    date_added = models.DateTimeField(default=datetime.now)
    contract = models.ForeignKey(Contracts, models.DO_NOTHING)
    item = models.ForeignKey(MdatItems, models.DO_NOTHING)
    quantity = models.DecimalField(max_digits=19, decimal_places=6)
    billed = models.BooleanField(default=False)
    reference = models.CharField(max_length=100, blank=True, null=True, default=None)

    class Meta:
        db_table = "contract_one_time_billing"


class ContractItemParentItem(models.Model):
    child_item = models.ForeignKey(MdatItems, models.DO_NOTHING, related_name="+")
    master_contract_item = models.ForeignKey(
        MdatItems, models.DO_NOTHING, related_name="+"
    )

    class Meta:
        db_table = "contract_item_parent_item"
        unique_together = (("child_item", "master_contract_item"),)


class ContractItemTerms(models.Model):
    item = models.OneToOneField(MdatItems, models.DO_NOTHING)
    billing_cycle = models.ForeignKey(ContractBillingCycles, models.DO_NOTHING)
    contract_terms = models.ForeignKey(ContractTerms, models.DO_NOTHING)

    class Meta:
        db_table = "contract_item_terms"


class ContractPartnerCommissions(models.Model):
    contract = models.ForeignKey(Contracts, models.DO_NOTHING)
    partner = models.ForeignKey(MdatCustomers, models.DO_NOTHING)
    commission_percent = models.DecimalField(
        max_digits=13,
        decimal_places=10,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100),
        ],
    )
    date_added = models.DateTimeField(default=datetime.now)
    start = models.DateField(default=date.today)
    end_raw = models.DateField(null=True, default=None)
    billed_until = models.DateField(default=date.today)

    @property
    def end(self):
        if self.contract.end is not None:
            if self.end_raw is not None:
                if self.contract.end < self.end_raw:
                    return self.contract.end
                else:
                    return self.end_raw
            else:
                return self.contract.end

        return self.end_raw

    @property
    def commission_percent_factor(self):
        if self.commission_percent <= 0:
            return 0

        return self.commission_percent / 100

    @property
    def commission(self):
        return self.contract.price * self.commission_percent_factor

    def save(self, *args, **kwargs):
        try:
            self.full_clean()
        except ValidationError as e:
            raise ValidationError

        super(ContractPartnerCommissions, self).save(*args, **kwargs)

    class Meta:
        db_table = "contract_partner_commissions"
        unique_together = (("contract", "partner", "start"),)
